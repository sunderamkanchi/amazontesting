import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/home/sunderam/Downloads/chromedriver_linux64/chromedriver");
        WebDriver driver = new ChromeDriver();
        searchProduct(driver);
        scrollAction(driver);
        addToCart(driver);
        mouseHover(driver);
        login(driver);
    }

   static void searchProduct(WebDriver driver) throws InterruptedException {
       driver.get("http://amazon.in");
       Thread.sleep(1000);
       driver.manage().window().maximize();
       Thread.sleep(1000);
       driver.findElement(By.id("twotabsearchtextbox")).sendKeys("atomic habits"+ Keys.ENTER);
   }

   static void scrollAction(WebDriver driver) throws InterruptedException {
       JavascriptExecutor js = (JavascriptExecutor) driver;
       Thread.sleep(2000);
       js.executeScript("window.scrollBy(0,400)");
   }

   static void addToCart(WebDriver driver) throws InterruptedException {
       Thread.sleep(1000);
       driver.findElement(By.linkText("Atomic Habits: The life-changing million copy bestseller")).click();
       ArrayList<String> all = new ArrayList<>(driver.getWindowHandles());
       driver.switchTo().window(all.get(1));
       driver.findElement(By.id("add-to-cart-button")).click();

   }

   static void mouseHover(WebDriver driver) throws InterruptedException {
       WebElement hover = driver.findElement(By.xpath("//a[@id='nav-link-accountList']"));
       Actions act = new Actions(driver);
       Thread.sleep(1000);
       act.moveToElement(hover).perform();
   }

   static void login(WebDriver driver){
        driver.findElement(By.xpath("//header/div[@id='navbar']/div[@id='nav-flyout-anchor']/div[@id='nav-flyout-accountList']/div[2]/div[1]/div[1]/div[1]/a[1]/span[1]")).click();
        driver.findElement(By.xpath("//input[@id='ap_email']")).sendKeys("luna@gmail.com");
   }


}